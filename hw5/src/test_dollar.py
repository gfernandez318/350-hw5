# testmystring.py

'''
This is the testing file using pytest. It tests all methods.
'''

from dollar import Dollar
from dollar import DollarList
import pytest  # only needed for the exception testing at the bottom
import sys


@pytest.fixture()
def mystring():
    return Dollar()
    return DollarList()


#This is testing the encrypt method. The character is encrypted
#to the chr value of a.
def test_init():
    doll = Dollar("AA12345678A", 5)
    assert doll.is_valid()

    with pytest.raises(ValueError):
        doll2 = Dollar("AZ12345678A", 5)


#This method tests the is_palindrome method. Three are to result
#in true; while one is to assert false.
def test_is_valid():
    doll = Dollar("AA12345678A", 5)
    doll.serial_number = "ZZ12345678Z"
    assert not doll.is_valid()

    doll2 = Dollar("AA12345678A", 5)
    doll2.denomination = 6
    assert not doll2.is_valid()


def test_eq():
    doll = Dollar("AA12345678A", 5)
    doll2 = Dollar("AA12345678A", 5)
    assert doll == doll2

    doll3 = Dollar("AA12345678A", 10)
    doll4 = Dollar("AA12345678A", 5)
    assert not doll3 == doll4


def test_lt():
    doll = Dollar("BB00345678A", 5)
    doll2 = Dollar("BB12345678A", 5)
    assert doll < doll2

    doll3 = Dollar("BB12345678A", 5)
    doll4 = Dollar("BB00345678A", 5)
    assert not doll3 < doll4


def test_repr():
    doll = Dollar("BB12345678B", 5)
    assert doll.__repr__() == "BB12345678B 5"


def test_copy():
    doll = Dollar("BB12345678B", 5)
    assert doll.copy() == Dollar("BB12345678B", 5)


def test_dollarlist_init():
    dl = DollarList('hot_sheet.txt')


#def test_from_list():
    #CLASSNAME.METHOD_NAME([list of dollars])


def test_dollarlist_len():
    dl = DollarList("")
    assert len(dl) == 0

    dl2 = DollarList('hot_sheet.txt')
    assert len(dl2) == 25


def test_dollarlist_repr():
    dl = DollarList("")
    assert "Empty Dollar list\n"

    #dl2 = DollarList.FROMLIST????("hot_sheet.txt")
    #assert repr(dl2) == "KI21489455W 20\nFB30710769V 5\n"


def test_rbsearch():
    #dl = DollarList('hot_sheet.txt')
    #assert dl.rbsearch(Dollar('FD89798529M', 10), 0, len(dl)) == 13

    #dl2 = DollarList('hot_sheet.txt')
    #assert dl2.rbsearch(Dollar('AF19683425I', 50), 0, len(dl2)-1) == 12

    dl3 = DollarList('hot_sheet.txt')
    assert dl3.rbsearch(Dollar('KI21489455X', 20), 0, len(dl3)) == -1


#def test_sintersection():
    #dl = DollarList('hot_sheet.txt')
    #dl2 = DollarList('hot_sheet2.txt')
    #assert dl.sintersection(Dollar('KI21489455W', 20))


#def test_bintersection():
    #dl = DollarList('hot_sheet.txt')
    #assert


#def test_intersection():
    #dl = DollarList('hot_sheet.txt')
    #assert


#def test_usesintersection():
    #dl = DollarList('hot_sheet.txt')
    #assert
