'''
Created on Apr 13, 2014

@author: Joseph Thibodeau and Gustavo Fernandez
'''
import math
import re
import functools


@functools.total_ordering
class Dollar:

    def __init__(self, serial='', denom=0):
        self.serial_number = serial
        self.denomination = int(denom)
        if not self.is_valid():
            raise ValueError("fake bill")

    def is_valid(self):
        denom_list = [5, 10, 20, 50, 100]
        if self.denomination in denom_list:
            x = re.search(r"[A-M][A-L]\d{8}[A-NP-Y]", self.serial_number)
            return x is not None
        else:
            return False

    def __eq__(self, other):
        if self.denomination == other.denomination:
            if self.serial_number == other.serial_number:
                return True
        return False

    def __lt__(self, other):
        if self.denomination <= other.denomination:
            if self.serial_number < other.serial_number:
                return True
        return False

    def __repr__(self):
        return self.serial_number + " " + str(self.denomination)

    def copy(self):
        return self


class DollarList():

    def __init__(self, filename=''):
        # N time minimum, possibly more depending on split's time.
        self.dollars = []
        if filename != '':
            serials = open(filename, 'r')

            lines = serials.read().splitlines()

            for line in lines:
                line = line.split()

            #for i in range(len(lines)):
                self.dollars.append(Dollar(line[0], line[1]))

            serials.close()

    # The instructions for this are super vague,
    # Makes sure it works right.
    @classmethod
    def from_list(cls, dollist):
        # This is constant time.
        listdollars = cls(dollist)

        return listdollars

    def __len__(self):
        # Constant time.
        return len(self.dollars)

    def __repr__(self):
        # This is just n time, for one loop.
        # Unless the default repr is more than constant.
        if not self.dollars:
            return "Empty Dollar List\n"
        else:
            result = ''
            for i in range(len(self.dollars)):
                result += (str(repr(self.dollars[i]) + '\n'))
            return result

    def rbsearch(self, search_dollar, left=0, right=None):
        if right is None:
            right = len(self.dollars)
        pos = (right + left) // 2

        if pos == len(self.dollars):
            return -1
        elif self.dollars[pos] == search_dollar:
            return pos
        elif left >= right:
            return -1
        elif self.dollars[pos] < search_dollar:
            return self.rbsearch(search_dollar, pos + 1, right)
        else:
            return self.rbsearch(search_dollar, left, pos)

    def sintersection(self, other):
        result = []

        # big O of n.
        for dollar in self.dollars:
            if dollar in other.dollars:
                result.append(dollar)

        return DollarList.from_list(result)

    def bintersection(self, other):
        result = []

        # big O of n with n to the n.
        # I think that means n ^ n squared.
        for item in other.dollars:
            if self.rbsearch(item) > -1:
                result.append(item)
        return result

    def intersection(self, other):
        m = len(self.dollars)
        n = len(other.dollars)
        self.dollars.sort()
        other.dollars.sort()
        # Not sure if we'll ever need the other
        # sorted, but just in case.

        if self.use_sintersection(m, n):
            return self.sintersection(other)
        else:
            return self.bintersection(other)

    # I'm not 100% sure of bintersection's big O time. LOG2!!!!!!!!!!!
    def use_sintersection(self, selflength, otherlength):
        sintime = selflength
        bintime = otherlength ** (selflength ** 2)

        if sintime <= bintime:
            return True


def main():  # pragma: no cover
    #dl2 = DollarList('hot_sheet.txt')
    #print(dl2)
    #print(dl2.rbsearch(Dollar('CB87382309J', 100), 0, len(dl2)-1))
    d4 = DollarList('hot_sheet.txt')
    dl3 = DollarList('hot_sheet2.txt')
    print(d4.sintersection(dl3))


if __name__ == '__main__':  # pragma: no cover
    main()
