'''
Created on Apr 13, 2014

@author: Joseph Thibodeau and Gustavo Fernandez
'''
#You may need to import Dollar here.
import math


class DollarList():

    def __init__(self, filename=''):
        # N time minimum, possibly more depending on split's time.
        self.dollars = []
        if filename != '':
            serials = open(filename, 'r')

            lines = serials.read().splitlines()

            for line in lines:
                line = line.split()

            for i in range(len(lines)):
                self.dollars.append(Dollar(lines[i][0], lines[i][1]))

            serials.close()

    # The instructions for this are super vague,
    # Makes sure it works right.
    @classmethod
    def from_list(cls, dollist):
        # This is constant time.
        listdollars = cls(dollist)

        return listdollars

    def __len__(self):
        # Constant time.
        return len(self.dollars)

    def __repr__(self):
        # This is just n time, for one loop.
        # Unless the default repr is more than constant.
        if not self.dollars:
            return "Empty Dollar List\n"
        else:
            result = []
            for i in range(len(self.dollars)):
                result.append(str(repr(self.dollars[i]) + '\n'))
            return result

    def rbsearch(self, search_dollar, left=0, right=None):

        if right == None:
            right = len(self.dollars)
        pos = left + (right - left) / len(self.dollars)

        if pos == len(search_dollar):
            return -1
        elif self.dollars[pos] == search_dollar:
            return pos
        elif left >= right:
            return -1
        elif self.dollars[pos] < search_dollar:
            return self.rbsearch(search_dollar, pos + 1, right)
        else:
            return self.rbsearch(search_dollar, left, pos)

    def sintersection(self, other):
        result = []

        # big O of n.
        for dollar in self.dollars:
            if dollar in other.dollars:
                result.append(dollar)

        return DollarList.from_list(result)

    def bintersection(self, other):
        result = []

        # big O of n with n to the n.
        # I think that means n ^ n squared.
        for item in other.dollars:
            if self.rbsearch(item) > -1:
                result.append(item)
        return result

    def intersection(self, other):
        m = len(self.dollars)
        n = len(other.dollars)
        self.dollars.sort()
        other.dollars.sort()
        # Not sure if we'll ever need the other
        # sorted, but just in case.

        if self.use_sintersection(m, n):
            return self.sintersection(other)
        else:
            return self.bintersection(other)

    # I'm not 100% sure of bintersection's big O time.
    def use_sintersection(self, selflength, otherlength):
        sintime = selflength
        bintime = otherlength ** (selflength ** 2)

        if sintime <= bintime:
            return True
